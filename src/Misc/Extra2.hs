{-# LANGUAGE ScopedTypeVariables #-}

module  Misc.Extra2
  ( Suffix
  , FileName (..)
  , FileBody
  , FileData
  , if'
  , headTailOp
  , headTailOp2
  , numMeanStdDev
  , numMeanStdDevFromMany
  , sglListToEither
  , changeFileExt
  , intercalateMon
  , ks2asFrom2Maps
  , funcOp
  , applyFuncToFileFameRoot
  , assertErr
  , noInfoStr
  , roundFloatStr
  ) where


import qualified Data.Map as M (Map, lookup)
import Data.List (intersperse)
import Text.Printf (PrintfArg, printf)
import GHC.Utils.Misc (fstOf3)
import Data.Either.Extra (maybeToEither)


type Suffix = String
data FileName = Str String | StdInOut deriving (Eq, Ord)
type FileBody = String
type FileData = (FileName, FileBody)


instance Show FileName where
  show (Str x)  = x
  show StdInOut = "StdInOut"

instance Semigroup FileName where
  (<>) (Str n1) (Str n2) = Str (n1 <> n2)
  (<>) (Str n1) StdInOut = Str n1
  (<>) StdInOut (Str n2) = Str n2
  (<>) StdInOut StdInOut = StdInOut

instance Monoid FileName where
  mempty = StdInOut


if' :: Bool -> a -> a -> a
if' b t f = if b then t else f


headTailOp :: (a -> b) -> (a -> b) -> [a] -> [b]
headTailOp _ _ [] = []
headTailOp op_head op_tail (x:xs) = op_head x : (op_tail <$> xs)


headTailOp2 :: (a -> b) -> (a -> b) -> [[a]] -> [[b]]
headTailOp2 op_head op_tail = headTailOp (headTailOp op_head op_tail) (op_tail <$>)


numMeanStdDev :: [Double] -> (Int, Double, Double)
numMeanStdDev nums = (n, mean, sqrt var)
  where
    n = length nums
    mean = sum nums / realToFrac n
    var = (sxsq / realToFrac n) - (mean ** 2)
    sxsq = sum (map (**2) nums)


numMeanStdDevFromMany :: [(Int, Double, Double)] -> (Int, Double, Double)
numMeanStdDevFromMany num_means_stdevs = (n, mean, sqrt var)
  where
    n = sum $ fstOf3 <$> num_means_stdevs
    mean = sx / realToFrac n
    var = (sxsq / realToFrac n) - (mean ** 2)
    sx = foldr (\(ni, mi, _) sxi -> (realToFrac ni * mi) + sxi) 0 num_means_stdevs
    sxsq = foldr (\(ni, mi, si) sxsqi -> (realToFrac ni * ((si ** 2) + (mi ** 2))) + sxsqi ) 0 num_means_stdevs


sglListToEither :: [a] -> Either String a
sglListToEither [] = Left "No elements"
sglListToEither [x] = Right x
sglListToEither _ = Left "Too many elements"


changeFileExt :: FileName -> String -> FileName
changeFileExt StdInOut _ = StdInOut
changeFileExt (Str name) ext = Str (takeWhile (/= '.') name ++ "." ++ ext)


intercalateMon :: (Monoid a) => a -> [a] -> a
intercalateMon x = mconcat . intersperse x


ks2asFrom2Maps :: forall k a. (Show k, Ord k) => String -> M.Map k a -> M.Map k (k -> a) -> [k]
               -> Either String [a]
ks2asFrom2Maps m_name map1 map2 ks = sequence $ getMult ks
  where
    getSingle key = maybeToEither (show key ++ " not a valid " ++ m_name) (M.lookup key map1)
    getMult :: [k] -> [Either String a]
    getMult [] = []
    getMult [x] = [getSingle x]
    getMult (x1:x2:xs) = case getDouble x1 x2 of
                           Just y  -> Right y : getMult xs
                           Nothing -> getSingle x1 : getMult (x2:xs)
    getDouble :: k -> k -> Maybe a
    getDouble k1 k2 = fmap ($ k2) (M.lookup k1 map2)


funcOp :: (Functor f) => f (a -> b) -> (f b -> c) -> a -> c
funcOp ops comb arg = comb $ fmap ($ arg) ops


applyFuncToFileFameRoot :: (String -> String) -> (FileName -> FileName)
applyFuncToFileFameRoot _ StdInOut = StdInOut
applyFuncToFileFameRoot func (Str name) = Str $ func file_root ++ file_ext
  where (file_root, file_ext) = break (== '.') name


assertErr :: a -> Bool -> Either a ()
assertErr x b = if not b then Left x else Right ()


noInfoStr :: String
noInfoStr = "~~~ No information found ~~~"


roundFloatStr :: (PrintfArg a) => Int -> a -> String
roundFloatStr sig_fig = printf ("%0." ++ show sig_fig ++ "f" )

