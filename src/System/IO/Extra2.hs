{-# LANGUAGE ScopedTypeVariables #-}

module System.IO.Extra2
  ( printGet
  , statelessIO
  , statefulIO
  ) where


import Control.Monad.State.Lazy (State, runState)


printGet :: String -> IO String
printGet str = putStrLn str >> getLine


statelessIO :: (String -> IO ()) -> IO ()
statelessIO process_func = mconcat $ fmap (>>= process_func) (repeat getLine)


statefulIO :: (String -> State s Bool) -> (s -> String) -> s -> IO s
statefulIO inputHandler state2str current_state  = do
  userIn <- printGet $ state2str current_state
  let (continue, new_state) = runState (inputHandler userIn) current_state
  if continue
     then statefulIO inputHandler state2str new_state
     else return new_state

