{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}

module Control.Monad.Extra2.Logger
    ( LogLevel (..)
    , LogEntry, LoggerT, MonadLogger
    , printLogs
    , debug, info, warn, err, critical
    ) where


import Control.Monad.Writer (MonadWriter, WriterT, tell)
import Data.List.Extra2 (ulines)


data LogLevel = LogDebug
              | LogInfo
              | LogWarn
              | LogError
              | LogCritical
              deriving (Eq, Ord, Show, Read)

type LogEntry = (LogLevel, String)
type LoggerT = WriterT [LogEntry]
type MonadLogger m = MonadWriter [LogEntry] m


logLevelChar :: LogLevel -> Char
logLevelChar LogDebug = 'D'
logLevelChar LogInfo = 'I'
logLevelChar LogWarn = 'W'
logLevelChar LogError = 'E'
logLevelChar LogCritical = 'C'


printLogs :: LogLevel -> [LogEntry] -> IO ()
printLogs minLvl logs = do
  let logLines = [logLevelChar lvl : " | " ++ msg | (lvl, msg) <- logs, lvl >= minLvl]
  case logLines of
    []        -> putStrLn $ "\nNo program logs at minimum level of " ++ show minLvl
    _logLines -> do
      putStrLn $ "\nProgram logs begin, minimum level is " ++ show minLvl
      putStrLn . ulines $ _logLines
      putStrLn "Program logs end\n"
      return ()


debug :: (MonadLogger m) => String -> m ()
debug msg = tell [(LogDebug, msg)]

info :: (MonadLogger m) => String -> m ()
info msg = tell [(LogInfo, msg)]

warn :: (MonadLogger m) => String -> m ()
warn msg = tell [(LogWarn, msg)]

err :: (MonadLogger m) => String -> m ()
err msg = tell [(LogError, msg)]

critical :: (MonadLogger m) => String -> m ()
critical msg = tell [(LogCritical, msg)]

