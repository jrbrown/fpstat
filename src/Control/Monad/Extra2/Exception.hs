{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Control.Monad.Extra2.Exception
    ( unwrapWithAct
    , unwrapToEmpty
    , LoggableException (..)
    , throwErrorLogged
    , unwrapThrowToIO
    , unwrapThrowToIOLogged
    , unwrapToEmptyLogged
    , ExceptGeneric (..)
    , ExceptNotImplemented (..)
    , ExceptProgramExit (..)
    , exitToIO
    , unwrapThrowToIOAndExit
    , unwrapThrowToIOLoggedAndExit
    , ExceptEmptyList (..)
    , lastE, headE, initE, tailE
    , maximumE, maximumByE
    , deleteE
    , lastLE, headLE, initLE, tailLE
    , maximumLE, maximumByLE
    , deleteLE
    , ExceptKeyNotFound (..)
    , lookupE
    , lookupLE
    ) where


import Data.List (maximumBy)
import Data.Map (Map, lookup)

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except (MonadError, ExceptT, throwError, runExceptT)
import Control.Monad.Writer (tell)
import Control.Monad.Extra2.Logger
    ( LogLevel (LogInfo, LogError, LogCritical)
    , MonadLogger
    , info, critical)



unwrapWithAct :: (Monad m) => (e -> m a) -> ExceptT e m a -> m a
unwrapWithAct excAct excT = runExceptT excT >>= f
    where f x = case x of
                  Right y                   -> return y
                  Left exc                  -> excAct exc


unwrapToEmpty :: (Monad m, Monoid a) => ExceptT e m a -> m a
unwrapToEmpty = unwrapWithAct (const $ return mempty)



class LoggableException a where
    severity :: a -> LogLevel
    description :: a -> String


throwErrorLogged :: (LoggableException e, MonadLogger m, MonadError e m) => e -> m a
throwErrorLogged e = tell [(severity e, "Exception: " ++ description e)] >> throwError e


mkErrorMsg :: LoggableException e => e -> String
mkErrorMsg exc = "Uncaught Exception: " ++ description exc


printError :: (MonadIO m, LoggableException e) => e -> m ()
printError = liftIO . putStrLn . mkErrorMsg


logAndPrintError :: (LoggableException e, MonadIO m, MonadLogger m) => e -> m ()
logAndPrintError exc = critical (mkErrorMsg exc) >> printError exc


unwrapThrowToIO :: (LoggableException e, MonadIO m) => ExceptT e m () -> m ()
unwrapThrowToIO = unwrapWithAct printError


unwrapThrowToIOLogged :: (LoggableException e, MonadIO m, MonadLogger m) => ExceptT e m () -> m ()
unwrapThrowToIOLogged = unwrapWithAct logAndPrintError


unwrapToEmptyLogged :: (MonadLogger m, LoggableException e, Monoid a) => ExceptT e m a -> m a
unwrapToEmptyLogged = unwrapWithAct (\e -> info ("Error turned to empty value: " ++ description e) >> return mempty)



-- A collection of (hopefully) useful error types and functions

newtype ExceptGeneric = ExceptGeneric String deriving (Eq, Ord, Show)

instance LoggableException ExceptGeneric where
    severity _ = LogError
    description (ExceptGeneric str) = str


data ExceptNotImplemented = ExceptNotImplemented deriving (Eq, Ord, Show)

instance LoggableException ExceptNotImplemented where
    severity _    = LogError
    description _ = "Functionality not implemented"



data ExceptProgramExit = GoodExit
                       | BadExit
                       deriving (Eq, Ord, Show)

instance LoggableException ExceptProgramExit where
    severity GoodExit = LogInfo
    severity BadExit = LogCritical
    description GoodExit = "Good program exit"
    description BadExit = "Bad program exit"


exitToIO :: (MonadIO m) => ExceptT ExceptProgramExit m a -> m (Maybe a)
exitToIO excT = do
    except_or_result <- runExceptT excT
    case except_or_result of
      Left GoodExit -> (liftIO . putStrLn) "Exiting..." >> return Nothing
      Left BadExit  -> (liftIO . putStrLn) "Bad exit!" >> return Nothing
      Right x       -> (return . Just) x


unwrapThrowToIOAndExit ::  (LoggableException e, MonadIO m, MonadError ExceptProgramExit m)
                       => ExceptT e m () -> m ()
unwrapThrowToIOAndExit = unwrapWithAct ((>> throwError BadExit) . printError)


unwrapThrowToIOLoggedAndExit :: ( LoggableException e
                                , MonadIO m
                                , MonadLogger m
                                , MonadError ExceptProgramExit m
                                ) => ExceptT e m () -> m ()
unwrapThrowToIOLoggedAndExit = unwrapWithAct ((>> throwError BadExit) . logAndPrintError )



newtype ExceptEmptyList = ExceptEmptyList String deriving (Eq, Ord, Show)

instance LoggableException ExceptEmptyList where
    severity _    = LogError
    description (ExceptEmptyList op) = "Tried " ++ op ++ " on an empty list"


mkListOpSafeGeneral :: (MonadError ExceptEmptyList m)
                    => (ExceptEmptyList -> m b) -> String -> ([a] -> b) -> [a] -> m b
mkListOpSafeGeneral errAct op _ [] = errAct $ ExceptEmptyList op
mkListOpSafeGeneral _ _ f xs  = return $ f xs

mkListOpSafeE :: (MonadError ExceptEmptyList m) => String -> ([a] -> b) -> [a] -> m b
mkListOpSafeE = mkListOpSafeGeneral throwError

mkListOpSafeLE :: (MonadLogger m, MonadError ExceptEmptyList m) => String -> ([a] -> b) -> [a] -> m b
mkListOpSafeLE = mkListOpSafeGeneral throwErrorLogged


lastE :: (MonadError ExceptEmptyList m) => [a] -> m a
lastE = mkListOpSafeE "lastE" last

lastLE :: (MonadLogger m, MonadError ExceptEmptyList m) => [a] -> m a
lastLE = mkListOpSafeLE "lastE" last


headE :: (MonadError ExceptEmptyList m) => [a] -> m a
headE = mkListOpSafeE "headE" head

headLE :: (MonadLogger m, MonadError ExceptEmptyList m) => [a] -> m a
headLE = mkListOpSafeLE "headE" head


maximumE :: (MonadError ExceptEmptyList m) => (Ord a) => [a] -> m a
maximumE = mkListOpSafeE "maximumE" maximum

maximumLE :: (MonadLogger m, MonadError ExceptEmptyList m) => (Ord a) => [a] -> m a
maximumLE = mkListOpSafeLE "maximumE" maximum


initE :: (MonadError ExceptEmptyList m) => [a] -> m [a]
initE = mkListOpSafeE "initE" init

initLE :: (MonadLogger m, MonadError ExceptEmptyList m) => [a] -> m [a]
initLE = mkListOpSafeLE "initE" init


tailE :: (MonadError ExceptEmptyList m) => [a] -> m [a]
tailE = mkListOpSafeE "tailE" tail

tailLE :: (MonadLogger m, MonadError ExceptEmptyList m) => [a] -> m [a]
tailLE = mkListOpSafeLE "tailE" tail


maximumByE :: (MonadError ExceptEmptyList m) => (a -> a -> Ordering) -> [a] -> m a
maximumByE f = mkListOpSafeE "maximumByE" (maximumBy f)

maximumByLE :: (MonadLogger m, MonadError ExceptEmptyList m)
            => (a -> a -> Ordering) -> [a] -> m a
maximumByLE f = mkListOpSafeLE "maximumByE" (maximumBy f)


deleteGeneral :: forall a m. (Eq a, MonadError ExceptEmptyList m)
              => (ExceptEmptyList -> m [a]) -> a -> [a] -> m [a]
deleteGeneral errAct = deleteGeneral' []
    where
        deleteGeneral' :: [a] -> a -> [a] -> m [a]
        deleteGeneral' _ _ [] = errAct $ ExceptEmptyList "deleteE"
        deleteGeneral' zs x (y:ys)
          | x == y    = return $ reverse zs ++ ys
          | otherwise = deleteGeneral' (y:zs) x ys

deleteE :: (Eq a, MonadError ExceptEmptyList m) => a -> [a] -> m [a]
deleteE = deleteGeneral throwError

deleteLE :: (Eq a, MonadLogger m, MonadError ExceptEmptyList m) => a -> [a] -> m [a]
deleteLE = deleteGeneral throwErrorLogged



newtype ExceptKeyNotFound = ExceptKeyNotFound String deriving (Eq, Ord, Show)

instance LoggableException ExceptKeyNotFound where
    severity _ = LogError
    description (ExceptKeyNotFound key) = "Key not found: " ++ key


lookupGeneral :: (Ord k, Show k, MonadError ExceptKeyNotFound m)
              => (ExceptKeyNotFound -> m a) -> k -> Map k a -> m a
lookupGeneral errAct k m =
    case Data.Map.lookup k m of
      Nothing -> errAct $ ExceptKeyNotFound (show k)
      Just x  -> return x

lookupE :: (Ord k, Show k, MonadError ExceptKeyNotFound m) => k -> Map k a -> m a
lookupE = lookupGeneral throwError

lookupLE :: (Ord k, Show k, MonadLogger m, MonadError ExceptKeyNotFound m) => k -> Map k a -> m a
lookupLE = lookupGeneral throwErrorLogged

