module Control.Monad.Extra2
  ( composeM
  , foldSeqM
  , replicateM
  , whileJust
  ) where


import Data.Foldable (fold)


composeM :: (Monad m) => (a -> b -> c) -> m a -> m b -> m c
composeM f m1 m2 = m1 >>= (\x1 -> f x1 <$> m2)

foldSeqM :: (Monad m, Traversable t, Monoid a) => t (m a) -> m a
foldSeqM = fmap fold . sequence

replicateM :: (Monad m) => Int -> m a -> m [a]
replicateM n mon = sequence $ replicate n mon

whileJust :: (Monad m) => m (Maybe a) -> m [a]
whileJust act = go
    where go = do
               res <- act
               f res
          f Nothing  = return []
          f (Just r) = do
                       rs <- go
                       return (r:rs)

