module Control.Monad.Trans.Maybe.Extra2
  ( hoistMaybe
  ) where

import Control.Monad.Trans.Maybe (MaybeT)
import Control.Monad (MonadPlus(mzero))


hoistMaybe :: (Monad m) => Maybe a -> MaybeT m a
hoistMaybe = maybe mzero return

