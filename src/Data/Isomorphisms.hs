{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}


module Data.Isomorphisms
  ( Iso
  , iso
  ) where


import Data.Either.Extra (maybeToEither, eitherToMaybe)


-- Need to define one for each direction
class Iso a b where
    iso    :: a -> b


instance {-# OVERLAPPABLE #-} Iso a a where
    iso = id

-- Haven't implemented reflexive and transitive isomorphism since it causes issues

--instance {-# OVERLAPPABLE #-} (Iso a b) => Iso b a where
--    iso = isoInv
--    isoInv = iso


instance (Iso c a, Iso b d) => Iso (a -> b) (c -> d) where
  iso f = iso . f . iso

instance Iso a (() -> a) where
  iso x _ = x
instance Iso (() -> a) a where
  iso f = f ()

instance Iso (a, b, c) (a, (b, c)) where
    iso (x, y, z) = (x, (y, z))
instance Iso (a, (b, c)) (a, b, c) where
    iso (x, (y, z)) = (x, y, z)

instance Iso (a, b, c) ((a, b), c) where
    iso (x, y, z) = ((x, y), z)
instance Iso ((a, b), c) (a, b, c) where
    iso ((x, y), z) = (x, y, z)

instance Iso (a, b, c, d) (a, (b, c, d)) where
    iso (x, y, z, w) = (x, (y, z, w))
instance Iso (a, (b, c, d)) (a, b, c, d) where
    iso (x, (y, z, w)) = (x, y, z, w)

instance Iso ((a, b) -> c) (a -> b -> c) where
  iso = curry
instance Iso (a -> b -> c) ((a, b) -> c) where
  iso = uncurry

instance Iso (Maybe a) (Either () a) where
  iso = maybeToEither ()
instance Iso (Either () a) (Maybe a) where
  iso = eitherToMaybe

