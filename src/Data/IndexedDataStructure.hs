{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Data.IndexedDataStructure
    ( Dict1
    , Dict2
    , Dict3
    , Dict4
    , Dict5
    , fetch
    ) where


import Data.List.Extra ((!?))
import qualified Data.Map as M


class IndexedDataStructure a b c where
    fetch :: a -> b -> c


type Dict1 a = M.Map String a
type Dict2 a = M.Map String (Dict1 a)
type Dict3 a = M.Map String (Dict2 a)
type Dict4 a = M.Map String (Dict3 a)
type Dict5 a = M.Map String (Dict4 a)

type String2 = (String, String)
type String3 = (String, String, String)
type String4 = (String, String, String, String)
type String5 = (String, String, String, String, String)


instance IndexedDataStructure String (Dict1 a) (Maybe a) where
    fetch = M.lookup

instance IndexedDataStructure String2 (Dict2 a) (Maybe a) where
    fetch (k1, k2) m = fetch k1 m >>= M.lookup k2

instance IndexedDataStructure String3 (Dict3 a) (Maybe a) where
    fetch (k1, k2, k3) m = fetch (k1, k2) m >>= M.lookup k3

instance IndexedDataStructure String4 (Dict4 a) (Maybe a) where
    fetch (k1, k2, k3, k4) m = fetch (k1, k2, k3) m >>= M.lookup k4

instance IndexedDataStructure String5 (Dict5 a) (Maybe a) where
    fetch (k1, k2, k3, k4, k5) m = fetch (k1, k2, k3, k4) m >>= M.lookup k5


type Int2 = (Int, Int)
type Int3 = (Int, Int, Int)
type Int4 = (Int, Int, Int, Int)
type Int5 = (Int, Int, Int, Int, Int)


instance IndexedDataStructure Int [a] (Maybe a) where
    fetch = flip (!?)

instance IndexedDataStructure Int2 [[a]] (Maybe a) where
    fetch (i1, i2) l = fetch i1 l >>= flip (!?) i2

instance IndexedDataStructure Int3 [[[a]]] (Maybe a) where
    fetch (i1, i2, i3) l = fetch (i1, i2) l >>= flip (!?) i3

instance IndexedDataStructure Int4 [[[[a]]]] (Maybe a) where
    fetch (i1, i2, i3, i4) l = fetch (i1, i2, i3) l >>= flip (!?) i4

instance IndexedDataStructure Int5 [[[[[a]]]]] (Maybe a) where
    fetch (i1, i2, i3, i4, i5) l = fetch (i1, i2, i3, i4) l >>= flip (!?) i5

