{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Distribution
    ( -- * Types
      CDDist (CDDist)
    , DDist
    , DDist'

    -- * Creating distributions
    , mkDist
    , mkBigramDist
    , mkUniform
    , fromWeightFunc
    , bigramFromPredSuccWeights
    , unigramFromAscFile
    , bigramFromAscFile

    -- * Manipulating disitributions
    , normalise
    , compress
    , distWithSequence
    , changeTmp

    -- * Selecting
    , select

    -- * Sampling from distributions
    , sample
    , sampleRm
    , sampleSeq
    , sampleSeqRm
    , sampleUntilEmpty
    , sampleSeqUntilEmpty

    -- * Stateful sampling
    , sampleSt
    , sampleRmSt
    , sampleSeqSt
    , sampleSeqRmSt
    , sampleUntilEmptySt
    , sampleSeqUntilEmptySt

    -- * Useful functions
    , permuteList
    ) where


import System.Random (RandomGen, random)
import Data.Bifunctor (first, second)
import Control.Monad (join)
import Control.Monad.State as S (State, state, runState)
import Control.Monad.Extra2 (whileJust)

import Data.Isomorphisms (Iso, iso)
import Data.List (nub)
import Data.List.Extra2 (dropElem, partition)
import Data.Bifunctor.Extra2 ((<$/>), (<$/$>))
import Data.Map (fromList, findWithDefault)
import Data.Map as M (lookup)
import Data.Tuple.Extra (dupe)
import Data.Maybe (fromMaybe)
import Data.Char (chr)


-- Types

newtype CDDist a b = CDDist [(b, a -> Double)]
newtype DDist' a = DDist' [(a, Double)] deriving (Eq, Ord, Show)
type DDist = CDDist ()

instance Show b => Show (CDDist a b) where
    show (CDDist xs) = "Conditional discrete distribution over:\n" ++ (xs >>= show . fst)


instance Iso (DDist' a) (CDDist () a) where
    iso (DDist' xs) = CDDist $ iso <$/> xs

instance Iso (CDDist () a) (DDist' a) where
    iso (CDDist xs) = DDist' $ iso <$/> xs


instance Functor (CDDist a) where
    fmap f (CDDist xs) = CDDist (first f <$> xs)

instance Applicative (CDDist a) where
    pure x = CDDist [(x, const 1)]
    (<*>) (CDDist fs) (CDDist xs) = CDDist . join $ nestList
        where nestList = [[(f x, \y -> px y * pf y) | (x,px) <- xs] | (f,pf) <- fs]

instance Monad (CDDist a) where
    (>>=) cdist op = join' $ op <$> cdist
        where
            join' :: CDDist a (CDDist a b) -> CDDist a b
            join' (CDDist xxs) = CDDist $ xxs >>= embedProbFunc
            embedProbFunc (CDDist xs, pf1) = (\pf2 y -> pf1 y * pf2 y) <$/> xs


-- Creating distributions

mkDist :: [(a, Double)] -> DDist a
mkDist = CDDist . (<$/>) iso

mkUniform :: [a] -> DDist a
mkUniform xs = iso . DDist' $ (, 1) <$> xs

fromWeightFunc :: [a] -> (a -> Double) -> DDist a
fromWeightFunc xs f = iso . DDist' $ (\x -> (x, f x)) <$> xs

mkBigramDist :: forall a. (Ord a) => [(a, Double)] -> [(a, [(a, Double)])] -> CDDist [a] a
mkBigramDist iniDist cndDists = CDDist $ second cndLookup . dupe <$> allElems
    where
        allElems = nub $ (fst <$> iniDist) ++ (fst <$> cndDists)
        (iniMap, cndMap) = (fromList iniDist, fromList (fromList <$/> cndDists))
        cndLookup :: a -> [a] -> Double
        cndLookup x [] = findWithDefault 0 x iniMap
        cndLookup x (y:_) = fromMaybe 0 $ M.lookup x cndMap >>= M.lookup y

bigramFromPredSuccWeights :: forall a. (Ord a) => [(a, a, Double)] -> CDDist [a] a
bigramFromPredSuccWeights psws = mkBigramDist iniDist cndDist
    where
        -- Flip c's as we go from pred succ to var condition
        spws = (\(p, s, w) -> (s, p, w)) <$> psws
        cndDist :: [(a, [(a, Double)])]
        cndDist = second normCnds <$> partition (iso <$> spws)
        iniDist :: [(a, Double)]
        iniDist = second (\ys -> sum $ snd <$> ys) <$> cndDist
        normCnds :: [(a, Double)] -> [(a, Double)]
        normCnds ys = second (/(sum $ snd <$> ys)) <$> ys

unigramFromAscFile :: String -> IO (Maybe (CDDist () Char))
unigramFromAscFile fname = do
    contents <- readFile fname
    return (CDDist <$> mapM (processWords . words) (lines contents))
        where
            processWords :: [String] -> Maybe (Char, () -> Double)
            processWords [code,count] = Just (chr (read code), const (read count))
            processWords _ = Nothing

bigramFromAscFile :: String -> IO (Maybe (CDDist String Char))
bigramFromAscFile fname = do
    contents <- readFile fname
    -- TODO: Make norming of conditionals optional and provide source for iniDistM 
    -- since it's essentially uniform atm
    let psws = mapM (processWords . words) (lines contents)
    return (bigramFromPredSuccWeights <$> psws)
        where
            processWords :: [String] -> Maybe (Char, Char, Double)
            processWords [c1,c2,count] = Just (chr (read c1), chr (read c2), read count)
            processWords _ = Nothing


-- * Manipulating disitributions

distWithSequence :: (Ord b) => CDDist a b -> [b] -> CDDist a b
distWithSequence (CDDist xs) ys = CDDist $ second getProbF . dupe <$> ys
    where getProbF k = findWithDefault (const 0) k (fromList xs)

probOp :: (Double -> Double) -> CDDist a b -> CDDist a b
probOp f (CDDist xs) = CDDist $ f <$/$> xs

changeTmp :: Double -> CDDist a b -> CDDist a b
changeTmp t = probOp (**t)

condition :: CDDist a b -> a -> DDist' b
condition (CDDist cdist) cond = DDist' $ ($ cond) <$/> cdist

normalise :: DDist' a -> DDist' a
normalise (DDist' xs) = DDist' $ second (/total) <$> xs
    where total = sum $ snd <$> xs

compress :: (Eq b) => CDDist a b -> CDDist a b
compress (CDDist xs) = CDDist $ compress' xs []
    where
        compress' :: (Eq b) => [(b, a -> Double)] -> [(b, a -> Double)] -> [(b, a -> Double)]
        compress' [] zs = zs
        compress' (y:ys) zs = compress' ys (compress'' y zs [])
        compress'' y [] ws = y:ws
        compress'' y@(ya, yp) (z@(za,zp):zs) ws
          | ya == za  = zs ++ (za, \c -> yp c + zp c):ws
          | otherwise = compress'' y zs (z:ws)


-- Selecting

select :: DDist' a -> Double -> Maybe (a, Int)
select = select' 0
    where
        select' :: Int -> DDist' a -> Double -> Maybe (a, Int)
        select' _ (DDist' []) _ = Nothing
        select' n (DDist' ((xa,xp):xs)) p
          | p <= xp   = Just (xa, n)
          | otherwise = select' (n + 1) (DDist' xs) (p - xp)


-- Sampling from distributions

sample :: (RandomGen g) => CDDist a b -> a -> g -> (Maybe b, g)
sample cdist c rng = (result, new_rng)
    where (p, new_rng) = random rng
          result = fst <$> select (normalise (condition cdist c)) p

sampleRm :: (RandomGen g) => CDDist a b -> a -> g -> (Maybe b, CDDist a b, g)
sampleRm cdist@(CDDist xs) c rng =
    case select (normalise (condition cdist c)) p of
      Just (result, n) -> (Just result, CDDist (dropElem n xs), new_rng)
      Nothing          -> (Nothing, cdist, new_rng)
    where (p, new_rng) = random rng

sampleSeq :: (RandomGen g) => CDDist [a] a -> [a] -> g -> (Maybe a, [a], g)
sampleSeq cdist xs rng = case sample cdist xs rng of
                           (Just x, new_rng)  -> (Just x, x:xs, new_rng)
                           (Nothing, new_rng) -> (Nothing, xs, new_rng)

sampleSeqRm :: (RandomGen g) => CDDist [a] a -> [a] -> g -> (Maybe a, CDDist [a] a, [a], g)
sampleSeqRm cdist xs rng = case sampleRm cdist xs rng of
                             (Just x, new_cdist, new_rng)  -> (Just x, new_cdist, x:xs, new_rng)
                             (Nothing, new_cdist, new_rng) -> (Nothing, new_cdist, xs, new_rng)

sampleUntilEmpty :: (RandomGen g) => a -> CDDist a b -> g -> ([b], g)
sampleUntilEmpty cond cdist rng = second snd $ runState op (cdist, rng)
    where op = whileJust $ sampleRmSt cond

sampleSeqUntilEmpty :: (RandomGen g) => CDDist [a] a -> g -> ([a], g)
sampleSeqUntilEmpty cdist rng = (result, new_rng)
    where op = whileJust sampleSeqRmSt
          (_, (_, result, new_rng)) = runState op (cdist, [], rng)


-- Stateful sampling

sampleSt :: (RandomGen g) => CDDist a b -> a -> State g (Maybe b)
sampleSt cdist = state . sample cdist

sampleRmSt :: (RandomGen g) => a -> State (CDDist a b, g) (Maybe b)
sampleRmSt cond = state $ \(cdist, rng) -> iso (sampleRm cdist cond rng)

sampleSeqSt :: (RandomGen g) => CDDist [a] a -> State ([a], g) (Maybe a)
sampleSeqSt cdist = state $ \(xs, rng) -> iso (sampleSeq cdist xs rng)

sampleSeqRmSt :: (RandomGen g) => State (CDDist [a] a, [a], g) (Maybe a)
sampleSeqRmSt = state $ \(cdist, xs, rng) -> iso (sampleSeqRm cdist xs rng)

sampleSeqUntilEmptySt :: (RandomGen g) => CDDist [a] a -> State g [a]
sampleSeqUntilEmptySt = state . sampleSeqUntilEmpty

sampleUntilEmptySt :: (RandomGen g) => a -> CDDist a b -> State g [b]
sampleUntilEmptySt cond = state . sampleUntilEmpty cond


-- Useful functions

permuteList :: (RandomGen g, Ord a) => g -> [a] -> [a]
permuteList rng xs = fst $ sampleUntilEmpty () ds rng
    where ds = distWithSequence (mkUniform (nub xs)) xs

