module Data.Either.Extra2
  ( eitherFail
  , leftToMaybe
  , rightToMaybe
  , maybeToLeft
  , maybeToRight
  ) where


eitherFail :: Either String a -> a
eitherFail (Left err) = error err
eitherFail (Right x) = x

leftToMaybe :: Either a b -> Maybe a
leftToMaybe (Left x)  = Just x
leftToMaybe (Right _) = Nothing

rightToMaybe :: Either a b -> Maybe b
rightToMaybe (Left _)  = Nothing
rightToMaybe (Right x) = Just x

maybeToLeft :: b -> Maybe a -> Either a b
maybeToLeft _ (Just x) = Left x
maybeToLeft x Nothing  = Right x

maybeToRight :: a -> Maybe b -> Either a b
maybeToRight _ (Just x) = Right x
maybeToRight x Nothing  = Left x

