module Data.List.Extra2
  ( ulines
  , dropElem
  , symetricDiff
  , partition
  , lastM
  , headM
  , initM
  , tailM
  , maximumM
  , maximumByM
  , deleteM
  ) where


import Data.List (union, intersect, (\\), nub, maximumBy, intercalate)


ulines :: [String] -> String
ulines = intercalate "\n"

dropElem :: Int -> [a] -> [a]
dropElem n xs = take n xs ++ drop (n+1) xs

symetricDiff :: (Eq a) => [a] -> [a] -> [a]
symetricDiff xs ys = union xs ys \\ intersect xs ys

partition :: (Eq a) => [(a, b)] -> [(a, [b])]
partition src = [ (t, [x | (t', x) <- src, t == t'])
                | t <- nub (fst <$> src) ]

mkListOpSafeM :: ([a] -> b) -> [a] -> Maybe b
mkListOpSafeM _ [] = Nothing
mkListOpSafeM f xs = Just $ f xs

lastM :: [a] -> Maybe a
lastM = mkListOpSafeM last

headM :: [a] -> Maybe a
headM = mkListOpSafeM head

maximumM :: (Ord a) => [a] -> Maybe a
maximumM = mkListOpSafeM maximum

initM :: [a] -> Maybe [a]
initM = mkListOpSafeM init

tailM :: [a] -> Maybe [a]
tailM = mkListOpSafeM tail

maximumByM :: (a -> a -> Ordering) -> [a] -> Maybe a
maximumByM f = mkListOpSafeM (maximumBy f)

deleteM :: (Eq a) => a -> [a] -> Maybe [a]
deleteM = deleteM' []
    where deleteM' _ _ [] = Nothing
          deleteM' zs x (y:ys)
            | x == y    = Just $ reverse zs ++ ys
            | otherwise = deleteM' (y:zs) x ys

