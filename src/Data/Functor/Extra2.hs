module Data.Functor.Extra2
  ( (<$$>)
  , (<$$$>)
  , (<$$$$>)
  , (<$$$$$>)
  ) where

infixl 5 <$$>
(<$$>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<$$>) = fmap . fmap

infixl 6 <$$$>
(<$$$>) :: (Functor f, Functor g, Functor h) => (a -> b) -> f (g (h a)) -> f (g (h b))
(<$$$>) = fmap . fmap . fmap

infixl 7 <$$$$>
(<$$$$>) :: (Functor f, Functor g, Functor h, Functor t)
         => (a -> b) -> f (g (h (t a))) -> f (g (h (t b)))
(<$$$$>) = fmap . fmap . fmap . fmap

infixl 8 <$$$$$>
(<$$$$$>) :: (Functor f, Functor g, Functor h, Functor t, Functor r)
         => (a -> b) -> f (g (h (t (r a)))) -> f (g (h (t (r b))))
(<$$$$$>) = fmap . fmap . fmap . fmap . fmap

