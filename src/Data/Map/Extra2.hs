module Data.Map.Extra2
  ( lookupFail
  ) where


import Data.Map (Map, lookup)
import Data.Maybe.Extra2 (maybeFail)


lookupFail :: (Ord k, Show k) => k -> Map k a -> a
lookupFail k = maybeFail (show k ++ " not found in map") . Data.Map.lookup k

