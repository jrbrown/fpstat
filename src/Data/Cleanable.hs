{-# LANGUAGE ScopedTypeVariables #-}

module Data.Cleanable
  ( Cleanable
  , cleanMaybe
  , cleanEither
  ) where


import Data.Map as M (Map, fromList, toList)
import Data.Maybe (mapMaybe, catMaybes)
import Data.Either.Extra (eitherToMaybe)
import Control.Monad (join)

import Data.Isomorphisms (iso)


class Functor f => Cleanable f where
    cleanMaybe :: forall a. f (Maybe a) -> f a
    cleanMaybe xs = cleanEither (iso <$> xs :: f (Either () a))
    cleanEither :: f (Either a b) -> f b
    cleanEither xs = cleanMaybe $ eitherToMaybe <$> xs
    {-# MINIMAL cleanMaybe | cleanEither #-}

instance Cleanable [] where
    cleanMaybe = catMaybes

instance Cleanable Maybe where
    cleanMaybe = join

instance Ord a => Cleanable (M.Map a) where
    cleanMaybe = M.fromList . mapMaybe sequence . M.toList

