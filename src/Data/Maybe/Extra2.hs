module Data.Maybe.Extra2
  ( maybeFail
  , catTML
  , catTMR
  ) where

maybeFail :: String -> Maybe a -> a
maybeFail str Nothing = error str
maybeFail _ (Just x) = x

-- catMaybe but for a tuple with maybe on left
catTML :: [(Maybe a, b)] -> [(a,b)]
catTML [] = []
catTML ((Just x,y):xs) = (x,y) : catTML xs
catTML ((Nothing, _):xs) = catTML xs

-- catMaybe but for a tuple with maybe on right
catTMR :: [(a, Maybe b)] -> [(a,b)]
catTMR [] = []
catTMR ((x,Just y):xs) = (x,y) : catTMR xs
catTMR ((_, Nothing):xs) = catTMR xs

