module Data.Bifunctor.Extra2
  ( (<\>), (</>)
  , (<$\>), (<$/>), (<\$>), (<\\>), (<\/>), (</$>), (</\>), (<//>)
  , (<$$\>), (<$$/>), (<$\$>), (<$\\>), (<$\/>), (<$/$>), (<$/\>), (<$//>)
  , (<\$$>), (<\$\>), (<\$/>), (<\\$>), (<\\\>), (<\\/>), (<\/$>), (<\/\>), (<\//>)
  , (</$$>), (</$\>), (</$/>), (</\$>), (</\\>), (</\/>), (<//$>), (<//\>), (<///>)
  , (<$/$/>)
  , (<$/$/$>), (<$/$/$/>)
  ) where


import Data.Bifunctor (Bifunctor, first, second)


-- Very simple
-- $ functor
-- \ bifunctor left/first
-- / bifunctor right/second
-- Appropriate function matches the top-to-bottom functor hierarchy
-- For example to change x in [((z, x), y)] you use <$\/>
-- We use the same properties as <$>, i.e. infixl 4 for single lifts
-- and then decrease priority for each level

infixl 4 <\>
(<\>) :: (Bifunctor f) => (a -> b) -> f a c -> f b c
(<\>) = first

infixl 4 </>
(</>) :: (Bifunctor f) => (a -> b) -> f c a -> f c b
(</>) = second

infixl 5 <$\>
(<$\>) :: (Functor f, Bifunctor g) => (a -> b) -> f (g a c) -> f (g b c)
(<$\>) = fmap . first

infixl 5 <$/>
(<$/>) :: (Functor f, Bifunctor g) => (a -> b) -> f (g c a) -> f (g c b)
(<$/>) = fmap . second

infixl 5 <\$>
(<\$>) :: (Bifunctor f, Functor g) => (a -> b) -> f (g a) c -> f (g b) c
(<\$>) = first . fmap

infixl 5 <\\>
(<\\>) :: (Bifunctor f, Bifunctor g) => (a -> b) -> f (g a c) d -> f (g b c) d
(<\\>) = first . first

infixl 5 <\/>
(<\/>) :: (Bifunctor f, Bifunctor g) => (a -> b) -> f (g c a) d -> f (g c b) d
(<\/>) = first . second

infixl 5 </$>
(</$>) :: (Bifunctor f, Functor g) => (a -> b) -> f c (g a) -> f c (g b)
(</$>) = second . fmap

infixl 5 </\>
(</\>) :: (Bifunctor f, Bifunctor g) => (a -> b) -> f c (g a d) -> f c (g b d)
(</\>) = second . first

infixl 5 <//>
(<//>) :: (Bifunctor f, Bifunctor g) => (a -> b) -> f c (g d a) -> f c (g d b)
(<//>) = second . second

infixl 6 <$$\>
(<$$\>) :: (Functor f, Functor g, Bifunctor h) => (a -> b) -> f (g (h a c)) -> f (g (h b c))
(<$$\>) = fmap . fmap . first

infixl 6 <$$/>
(<$$/>) :: (Functor f, Functor g, Bifunctor h) => (a -> b) -> f (g (h c a)) -> f (g (h c b))
(<$$/>) = fmap . fmap . second

infixl 6 <$\$>
(<$\$>) :: (Functor f, Bifunctor g, Functor h) => (a -> b) -> f (g (h a) c) -> f (g (h b) c)
(<$\$>) = fmap . first . fmap

infixl 6 <$\\>
(<$\\>) :: (Functor f, Bifunctor g, Bifunctor h) => (a -> b) -> f (g (h a c) d) -> f (g (h b c) d)
(<$\\>) = fmap . first . first

infixl 6 <$\/>
(<$\/>) :: (Functor f, Bifunctor g, Bifunctor h) => (a -> b) -> f (g (h c a) d) -> f (g (h c b) d)
(<$\/>) = fmap . first . second

infixl 6 <$/$>
(<$/$>) :: (Functor f, Bifunctor g, Functor h) => (a -> b) -> f (g c (h a)) -> f (g c (h b))
(<$/$>) = fmap . second . fmap

infixl 6 <$/\>
(<$/\>) :: (Functor f, Bifunctor g, Bifunctor h) => (a -> b) -> f (g c (h a d)) -> f (g c (h b d))
(<$/\>) = fmap . second . first

infixl 6 <$//>
(<$//>) :: (Functor f, Bifunctor g, Bifunctor h) => (a -> b) -> f (g c (h d a)) -> f (g c (h d b))
(<$//>) = fmap . second . second

infixl 6 <\$$>
(<\$$>) :: (Bifunctor f, Functor g, Functor h) => (a -> b) -> f (g (h a)) c -> f (g (h b)) c
(<\$$>) = first . fmap . fmap

infixl 6 <\$\>
(<\$\>) :: (Bifunctor f, Functor g, Bifunctor h) => (a -> b) -> f (g (h a c)) d -> f (g (h b c)) d
(<\$\>) = first . fmap . first

infixl 6 <\$/>
(<\$/>) :: (Bifunctor f, Functor g, Bifunctor h) => (a -> b) -> f (g (h c a)) d -> f (g (h c b)) d
(<\$/>) = first . fmap . second

infixl 6 <\\$>
(<\\$>) :: (Bifunctor f, Bifunctor g, Functor h) => (a -> b) -> f (g (h a) c) d -> f (g (h b) c) d
(<\\$>) = first . first . fmap

infixl 6 <\\\>
(<\\\>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f (g (h a c) d) e -> f (g (h b c) d) e
(<\\\>) = first . first . first

infixl 6 <\\/>
(<\\/>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f (g (h c a) d) e -> f (g (h c b) d) e
(<\\/>) = first . first . second

infixl 6 <\/$>
(<\/$>) :: (Bifunctor f, Bifunctor g, Functor h) => (a -> b) -> f (g c (h a)) e -> f (g c (h b)) e
(<\/$>) = first . second . fmap

infixl 6 <\/\>
(<\/\>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f (g c (h a d)) e -> f (g c (h b d)) e
(<\/\>) = first . second . first

infixl 6 <\//>
(<\//>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f (g c (h d a)) e -> f (g c (h d b)) e
(<\//>) = first . second . second

infixl 6 </$$>
(</$$>) :: (Bifunctor f, Functor g, Functor h) => (a -> b) -> f c (g (h a)) -> f c (g (h b))
(</$$>) = second . fmap . fmap

infixl 6 </$\>
(</$\>) :: (Bifunctor f, Functor g, Bifunctor h) => (a -> b) -> f d (g (h a c)) -> f d (g (h b c))
(</$\>) = second . fmap . first

infixl 6 </$/>
(</$/>) :: (Bifunctor f, Functor g, Bifunctor h) => (a -> b) -> f d (g (h c a)) -> f d (g (h c b))
(</$/>) = second . fmap . second

infixl 6 </\$>
(</\$>) :: (Bifunctor f, Bifunctor g, Functor h) => (a -> b) -> f d (g (h a) c) -> f d (g (h b) c)
(</\$>) = second . first . fmap

infixl 6 </\\>
(</\\>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f e (g (h a c) d) -> f e (g (h b c) d)
(</\\>) = second . first . first

infixl 6 </\/>
(</\/>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f e (g (h c a) d) -> f e (g (h c b) d)
(</\/>) = second . first . second

infixl 6 <//$>
(<//$>) :: (Bifunctor f, Bifunctor g, Functor h) => (a -> b) -> f e (g c (h a)) -> f e (g c (h b))
(<//$>) = second . second . fmap

infixl 6 <//\>
(<//\>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f e (g c (h a d)) -> f e (g c (h b d))
(<//\>) = second . second . first

infixl 6 <///>
(<///>) :: (Bifunctor f, Bifunctor g, Bifunctor h)
        => (a -> b) -> f e (g c (h d a)) -> f e (g c (h d b))
(<///>) = second . second . second

infixl 7 <$/$/>
(<$/$/>) :: (Functor f, Bifunctor g, Functor h, Bifunctor t)
         => (a -> b) -> f (g c (h (t d a))) -> f (g c (h (t d b)))
(<$/$/>) = fmap . second . fmap . second

infixl 8 <$/$/$>
(<$/$/$>) :: (Functor f, Bifunctor g, Functor h, Bifunctor t, Functor q)
         => (a -> b) -> f (g c (h (t d (q a)))) -> f (g c (h (t d (q b))))
(<$/$/$>) = fmap . second . fmap . second . fmap

infixl 9 <$/$/$/>
(<$/$/$/>) :: (Functor f, Bifunctor g, Functor h, Bifunctor t, Functor q, Bifunctor p)
         => (a -> b) -> f (g c (h (t d (q (p e a))))) -> f (g c (h (t d (q (p e b)))))
(<$/$/$/>) = fmap . second . fmap . second . fmap . second

